import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Betrag eingeben");
		double betrag = in.nextDouble();
		
		System.out.println("Steuersatz eingeben (j) = ermässigt : (n) = normal");
		char steuersatz = in.next().charAt(0);
		
		switch (steuersatz) {
		
		case 'j' :
			System.out.println("Der ermässigte Steuersatz lautet bei: " + betrag + ": " + (betrag * 0.07));
			break;
		
		case 'n':
		    System.out.println("Der normale Steuersatz lautet bei: " + betrag + ": " + (betrag * 0.19));
		    break;
		
		default:
			System.out.println("Falsche Eingabe");
		

		}

	}

}
